(require 'package)
(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/") t)
(package-initialize)

(when (not (package-installed-p 'use-package))
  (package-refresh-contents)
  (package-install 'use-package))

(setq use-package-always-ensure t)

(use-package auto-compile
	     :config (auto-compile-on-load-mode))

(setq load-prefer-newer t)

(setq user-full-name "chris addy"
      user-mail-address "chrisaddy@pm.me")

(use-package evil
	     :config
	     (evil-mode 1))

(use-package evil-escape
  :config
  (evil-escape-mode 1))

(setq-default evil-escape-key-sequence "kj")

(evil-commentary-mode)

;; appearance
(load-theme 'dracula t)
(tool-bar-mode 0)
(menu-bar-mode 0)
(scroll-bar-mode -1)
(set-window-scroll-bars (minibuffer-window) nil nil)
(setq frame-title-format '((:eval (projectile-project-name))))

(global-prettify-symbols-mode t)

(global-linum-mode t)
(setq linum-format "%d ")

(use-package linum-relative)
(linum-relative-on)

(use-package moody
  :config
  (setq x-underline-at-descent-line t)
  (moody-replace-mode-line-buffer-identification)
  (moody-replace-vc-mode))

;; (global-hl-line-mode)

(use-package diff-hl
  :config
  (add-hook 'prog-mode-hook 'turn-on-diff-hl-mode)
  (add-hook 'vc-dir-mode-hook 'turn-on-diff-hl-mode))

(use-package gnus)

;; project management
(use-package ag)

(use-package company)
(add-hook 'after-init-hook 'global-company-mode)
(global-set-key (kbd "M-/") 'company-complete-common)

(use-package flycheck)

(use-package magit
  :bind
  ("C-x g" . magit-status)

  :config
  (use-package evil-magit)
  (use-package with-editor)
  (setq magit-push-always-verify nil)
  (setq git-commit-summary-max-length 50)

  (with-eval-after-load 'magit-remote
    (magit-define-popup-action 'magit-push-popup ?P
			       'magit-push-implicitly--desc
			       'magit-push-implicitly ?p t))

  (add-hook 'with-editor-mode-hook 'evil-insert-state))

(use-package ghub)
;;(use-package forge)

;; programming
(use-package subword
  :config (global-subword-mode 1))

(setq compilation-scroll-output t)

;; GO
(use-package go-mode)
(use-package go-errcheck)
(use-package company-go)

;; Haskell
(use-package haskell-mode)
(add-hook 'haskell-mode-hook
	  (lambda ()
	    (haskell-doc-mode)
	    (turn-on-haskell-indent)))

;; Python
(use-package python-mode)

(use-package elpy)
(elpy-enable)

(add-hook 'elpy-mode-hook 'flycheck-mode)

;; pep8 on save
(use-package py-autopep8)
(require 'py-autopep8)
(add-hook 'elpy-mode-hook 'py-autopep8-enable-on-save)

(use-package company-jedi)
(add-to-list 'company-backends 'company-jedi)

(add-hook 'python-mode-hook 'jedi:setup)
(setq jedi:complete-on-dot t)

(flycheck-define-checker
    python-mypy ""
    :command ("mypy"
              "--ignore-missing-imports" "--fast-parser"
              "--python-version" "3.6"
              source-original)
    :error-patterns
    ((error line-start (file-name) ":" line ": error:" (message) line-end))
    :modes python-mode)

(add-to-list 'flycheck-checkers 'python-mypy t)
(flycheck-add-next-checker 'python-pylint 'python-mypy t)

;; yaml
(use-package yaml-mode)


;; terminal
(use-package multi-term)
(global-set-key (kbd "C-c t") 'multi-term)

(evil-set-initial-state 'term-mode 'emacs)

(use-package org)

(use-package org-bullets
  :init
  (add-hook 'org-mode-hook 'org-bullets-mode))

(setq org-ellipsis "⤵")

(setq org-src-fontify-natively t)

(setq org-src-tab-acts-natively t)

(setq org-src-window-setup 'current-window)

(require 'ox-md)
(require 'ox-beamer)

(use-package gnuplot)
(org-babel-do-load-languages
 'org-babel-load-languages
 '((emacs-lisp . t)
   (python . t)
   (haskell . t)))

(setq org-confirm-babel-evaluate nil)

;; RSS
(use-package elfeed)
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(package-selected-packages
   (quote
    (org-drill-table elfeed gnuplot org-bullets ac-haskell-process flycheck-clojure flycheck-elixir flycheck-elm flycheck-ghcmod flycheck-golangci-lint flycheck-haskell flycheck-ledger flycheck-mypy flycheck-pycheckers flycheck-pyflakes flycheck-rebar3 flycheck-stack flymake-elixir flymake-go flymake-haskell-multi flymake-hlint flymake-python-pyflakes flymake-racket flymake-shell focus ghc ghc-imported-from ghci-completion haskell-emacs haskell-emacs-base haskell-emacs-text haskell-snippets haskell-tab-indent hasklig-mode hasky-extensions hasky-stack hi2 hindent hyai intero lsp-haskell shm ac-c-headers multi-term yaml-mode company-jedi forge evil-escape evil auto-compile use-package))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )



(setq-default cursor-type 'bar)
(setq evil-insert-state-cursor '((bar . 5) "yellow")
      evil-normal-state-cursor '(box "purple"))
