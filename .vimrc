" essentials
set nocompatible
filetype plugin on

" Plugins
call plug#begin('~/.vim/plugged')
Plug 'scrooloose/nerdtree', { 'on':  'NERDTreeToggle' }
Plug 'tpope/vim-fugitive'
Plug 'tpope/vim-surround'
Plug 'vim-airline/vim-airline'
Plug 'nvie/vim-flake8'
Plug 'fatih/vim-go'
Plug 'ervandew/supertab'
Plug 'bronson/vim-trailing-whitespace'
Plug 'vim-scripts/Zoomwin'
Plug 'w0rp/ale'
Plug 'altercation/vim-colors-solarized'
" Plug 'dracula/vim',{'as':'dracula'}
call plug#end()

" colors
syntax enable
" colorscheme dracula

syntax on

" a better escape for my tiny hands
imap kj <Esc>

" color width
" set colorcolumn=80
let w:m1=matchadd('ErrorMsg', '\%>80v.\+', -1)

" no cheating!
noremap  <Up> <Nop>
noremap  <Down> <Nop>
noremap  <Left> <Nop>
noremap  <Right> <Nop>

set number relativenumber

set hlsearch
